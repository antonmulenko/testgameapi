package com.game.core;

import com.game.core.config.Config;
import com.game.core.repository.DefaultLeaderboardRepositoryExt;
import com.game.core.repository.DefaultUserRepositoryExt;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class UserTest {

    private DefaultLeaderboardRepositoryExt leaderboardRepositoryExt = new DefaultLeaderboardRepositoryExt();
    private DefaultUserRepositoryExt userRepositoryExt = new DefaultUserRepositoryExt();

    @Before
    public void setUp() {
        GameContext.getInstance().setLeaderboardRepository(leaderboardRepositoryExt);
        GameContext.getInstance().setUsersRepository(userRepositoryExt);
        GameContext.getInstance().getConfig().setLeaderboardLength(2);
    }

    @After
    public void tearDown() {
        GameContext.getInstance().getConfig().setLeaderboardLength(Config.DEFAULT_LEADERBOARD_LENGTH);
        leaderboardRepositoryExt.clear();
        userRepositoryExt.clear();
    }

    @Test
    public void addNotPalindrom() {
        GameCoreApi api = new GameCore();

        String userId = "userId_1";
        String palindrom = "Asd dsa";
        String notPalindrom = "ASDasd";

        Assert.assertTrue(api.addPalindrom(userId, palindrom));
        Assert.assertEquals(palindrom.length(), api.getUserScore(userId));

        Assert.assertFalse(api.addPalindrom(userId, notPalindrom));
        Assert.assertEquals(palindrom.length(), api.getUserScore(userId));
    }

    @Test
    public void addPalindromsForOneUser() {
        GameCoreApi api = new GameCore();

        String userId = "userId_1";
        String palindrom1 = "bhg GHb";
        String palindrom2 = "Bhg Fuf ghb";

        Assert.assertTrue(api.addPalindrom(userId, palindrom1));
        Assert.assertEquals(palindrom1.length(), api.getUserScore(userId));

        Assert.assertTrue(api.addPalindrom(userId, palindrom2));
        Assert.assertEquals(palindrom1.length() + palindrom2.length(), api.getUserScore(userId));
    }

    @Test
    public void addSamePalindroms() {
        GameCoreApi api = new GameCore();

        String userId = "userId_1";
        String palindrom1 = "bhg GHb";
        String palindrom2 = "bhg GHb";

        Assert.assertTrue(api.addPalindrom(userId, palindrom1));
        Assert.assertEquals(palindrom1.length(), api.getUserScore(userId));

        Assert.assertFalse(api.addPalindrom(userId, palindrom2));
        Assert.assertEquals(palindrom1.length(), api.getUserScore(userId));
    }

    @Test
    public void addPalindromsForManyUser() {
        GameCoreApi api = new GameCore();

        String userId1 = "userId_1";
        String palindrom1User1 = "cDf fDc";
        String palindrom2User1 = "cDg Fuf gDc";

        String userId2 = "userId_2";
        String palindrom1User2 = "cdh hDc";
        String palindrom2User2 = "cgg Fuf ggc";

        Assert.assertTrue(api.addPalindrom(userId1, palindrom1User1));
        Assert.assertEquals(palindrom1User1.length(), api.getUserScore(userId1));

        Assert.assertTrue(api.addPalindrom(userId1, palindrom2User1));
        Assert.assertEquals(palindrom1User1.length() + palindrom2User1.length(), api.getUserScore(userId1));

        Assert.assertTrue(api.addPalindrom(userId2, palindrom1User2));
        Assert.assertEquals(palindrom1User2.length(), api.getUserScore(userId2));

        Assert.assertTrue(api.addPalindrom(userId2, palindrom2User2));
        Assert.assertEquals(palindrom1User2.length() + palindrom2User2.length(), api.getUserScore(userId2));
    }
}