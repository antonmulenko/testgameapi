package com.game.core;

import com.game.core.config.Config;
import com.game.core.models.UserScoreModel;
import com.game.core.repository.DefaultLeaderboardRepositoryExt;
import com.game.core.repository.DefaultUserRepositoryExt;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

public class LeaderboardTest {

    private DefaultLeaderboardRepositoryExt leaderboardRepositoryExt = new DefaultLeaderboardRepositoryExt();
    private DefaultUserRepositoryExt userRepositoryExt = new DefaultUserRepositoryExt();

    @Before
    public void setUp() {
        GameContext.getInstance().setLeaderboardRepository(leaderboardRepositoryExt);
        GameContext.getInstance().setUsersRepository(userRepositoryExt);
        GameContext.getInstance().getConfig().setLeaderboardLength(2);
    }

    @After
    public void tearDown() {
        GameContext.getInstance().getConfig().setLeaderboardLength(Config.DEFAULT_LEADERBOARD_LENGTH);
        leaderboardRepositoryExt.clear();
        userRepositoryExt.clear();
    }

    @Test
    public void getLeaderooard() {
        GameCoreApi api = new GameCore();

        String userId = "userId_1";
        String palindrom1 = "Asd dsa";
        String palindrom2 = "Asd1 1dsa";

        Assert.assertTrue(api.addPalindrom(userId, palindrom1));
        Assert.assertTrue(api.addPalindrom(userId, palindrom2));

        List<UserScoreModel> leaderboard = api.getLeaderboard();
        Assert.assertEquals(1, leaderboard.size());

        UserScoreModel scoreModel = leaderboard.get(0);
        Assert.assertEquals(userId, scoreModel.getUserId());
        Assert.assertEquals(palindrom1.length() + palindrom2.length(), scoreModel.getUserScore());
    }

    @Test
    public void leaderooardSort() {
        GameCoreApi api = new GameCore();

        String userId1 = "userId_1";
        String palindrom11 = "Asd b dsa";
        String palindrom12 = "Asd1 b 1dsa";

        String userId2 = "userId_2";
        String palindrom21 = "Asd2 b 2dsa";
        String palindrom22 = "Asd12 b 21dsa";

        Assert.assertTrue(api.addPalindrom(userId1, palindrom11));
        Assert.assertTrue(api.addPalindrom(userId1, palindrom12));

        Assert.assertTrue(api.addPalindrom(userId2, palindrom21));
        Assert.assertTrue(api.addPalindrom(userId2, palindrom22));

        List<UserScoreModel> leaderboard = api.getLeaderboard();
        Assert.assertEquals(2, leaderboard.size());

        UserScoreModel scoreModel = leaderboard.get(0);
        Assert.assertEquals(userId2, scoreModel.getUserId());
        Assert.assertEquals(palindrom21.length() + palindrom22.length(), scoreModel.getUserScore());
    }

    @Test
    public void leaderboardReplaceScores() {
        GameCoreApi api = new GameCore();

        String userId1 = "userId_1";
        String palindrom11 = "Asd b dsa";
        String palindrom12 = "Asd1 b 1dsa";

        String userId2 = "userId_2";
        String palindrom21 = "Asd2 b 2dsa";
        String palindrom22 = "Asd12 b 21dsa";

        String userId3 = "userId_3";
        String palindrom31 = "Asd23 b 32dsa";
        String palindrom32 = "Asd123 b 321dsa";

        Assert.assertTrue(api.addPalindrom(userId1, palindrom11));
        Assert.assertTrue(api.addPalindrom(userId1, palindrom12));

        Assert.assertTrue(api.addPalindrom(userId2, palindrom21));
        Assert.assertTrue(api.addPalindrom(userId2, palindrom22));

        Assert.assertTrue(api.addPalindrom(userId3, palindrom31));
        Assert.assertTrue(api.addPalindrom(userId3, palindrom32));

        List<UserScoreModel> leaderboard = api.getLeaderboard();
        Assert.assertEquals(GameContext.getInstance().getConfig().getLeaderboardLength(), leaderboard.size());

        Assert.assertEquals(userId3, leaderboard.get(0).getUserId());
        Assert.assertEquals(palindrom31.length() + palindrom32.length(), leaderboard.get(0).getUserScore());

        Assert.assertEquals(userId2, leaderboard.get(1).getUserId());
        Assert.assertEquals(palindrom21.length() + palindrom22.length(), leaderboard.get(1).getUserScore());
    }
}
