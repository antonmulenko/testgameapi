package com.game.core;

import com.game.core.config.Config;
import com.game.core.models.UserScoreModel;
import com.game.core.repository.DefaultLeaderboardRepositoryExt;
import com.game.core.repository.DefaultUserRepositoryExt;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

public class MultithreadingTest {

    private DefaultLeaderboardRepositoryExt leaderboardRepositoryExt = new DefaultLeaderboardRepositoryExt();
    private DefaultUserRepositoryExt userRepositoryExt = new DefaultUserRepositoryExt();

    @Before
    public void setUp() {
        GameContext.getInstance().setLeaderboardRepository(leaderboardRepositoryExt);
        GameContext.getInstance().setUsersRepository(userRepositoryExt);
        GameContext.getInstance().getConfig().setLeaderboardLength(Config.DEFAULT_LEADERBOARD_LENGTH);
    }

    @After
    public void tearDown() {
        GameContext.getInstance().getConfig().setLeaderboardLength(Config.DEFAULT_LEADERBOARD_LENGTH);
        leaderboardRepositoryExt.clear();
        userRepositoryExt.clear();
    }

    @Test
    public void multithreading() throws ExecutionException, InterruptedException {
        ExecutorService executor = Executors.newFixedThreadPool(100);

        List<Future<Long>> list = new ArrayList<>();
        for (int i = 0; i < 10000; i++) {
            list.add(executor.submit(() -> {
                addPalindrom();
                addPalindrom();
                return Calendar.getInstance().getTimeInMillis();
            }));
        }

        for (Future<Long> future : list) {
            future.get();
        }

        System.out.println("\nleaderboard length: " + GameContext.getInstance().getConfig().getLeaderboardLength());

        GameCoreApi api = new GameCore();
        List<UserScoreModel> leaderboard = api.getLeaderboard();
        Assert.assertEquals(GameContext.getInstance().getConfig().getLeaderboardLength(), leaderboard.size());

        for (UserScoreModel scoreModel : leaderboard) {
            System.out.println("scoremodel: " + scoreModel.getUserId() + ", " + scoreModel.getUserScore());
        }
    }

    private void addPalindrom() throws InterruptedException {
        GameCoreApi api = new GameCore();

        String userId = "userId_" + new Random().nextInt(20);
        String palindrom = "Asd " + new Random().nextInt(10000) + " dsa";

        TimeUnit.MICROSECONDS.sleep(new Random().nextInt(1000));

        api.addPalindrom(userId, palindrom);

        TimeUnit.MICROSECONDS.sleep(new Random().nextInt(1000));

        System.out.println("userId: " + userId + " palindrom: " + palindrom + " leaderboard length: " + api.getLeaderboard().size());
    }
}
