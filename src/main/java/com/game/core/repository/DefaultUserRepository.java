package com.game.core.repository;

import com.game.core.models.UserModel;

import java.util.HashMap;
import java.util.Map;

public class DefaultUserRepository implements UsersRepository {

    final Map<String, UserModel> userMap = new HashMap<>();

    @Override
    synchronized public void addUserModel(UserModel userModel) {
        userMap.put(userModel.getUserId(), userModel);
    }

    @Override
    synchronized public UserModel getUserModel(String userId) {
        if (userMap.containsKey(userId)) {
            return userMap.get(userId);
        }

        UserModel userModel = new UserModel();
        userModel.setUserId(userId);
        return userModel;
    }

    @Override
    synchronized public int getUserScore(String userId) {
        if (!userMap.containsKey(userId)) {
            return 0;
        }

        return userMap.get(userId).getUserScore();
    }
}
