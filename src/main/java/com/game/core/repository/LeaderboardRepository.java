package com.game.core.repository;

import com.game.core.models.UserScoreModel;

import java.util.List;

public interface LeaderboardRepository {
    void addUserScore(UserScoreModel userScoreModel);

    List<UserScoreModel> getLeaderboard();
}
