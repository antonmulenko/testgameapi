package com.game.core.repository;

import com.game.core.GameContext;
import com.game.core.models.UserScoreModel;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;

public class DefaultLeaderboardRepository implements LeaderboardRepository {

    final List<UserScoreModel> userScoreList = new ArrayList<>();

    @Override
    synchronized public void addUserScore(UserScoreModel userScoreModel) {
        UserScoreModel model = userScoreList.stream()
                .filter(u -> Objects.equals(userScoreModel.getUserId(), u.getUserId()))
                .findAny()
                .orElse(null);

        if (model != null) {
            model.setUserScore(userScoreModel.getUserScore());
            return;
        }

        UserScoreModel minScoreModel = userScoreList.stream()
                .min(Comparator.comparingInt(UserScoreModel::getUserScore))
                .orElse(null);

        if (minScoreModel == null ||
                userScoreList.size() < GameContext.getInstance().getConfig().getLeaderboardLength()) {
            userScoreList.add(userScoreModel);
            return;
        }

        if (userScoreModel.getUserScore() <= minScoreModel.getUserScore()) {
            return;
        }

        userScoreList.add(userScoreModel);
        userScoreList.remove(minScoreModel);
    }

    @Override
    synchronized public List<UserScoreModel> getLeaderboard() {
        userScoreList.sort(Comparator.comparingInt(UserScoreModel::getUserScore).reversed());
        return userScoreList;
    }
}
