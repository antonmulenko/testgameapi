package com.game.core.repository;

import com.game.core.models.UserModel;

public interface UsersRepository {
    int getUserScore(String userId);

    void addUserModel(UserModel userPalindromModel);

    UserModel getUserModel(String userId);
}
