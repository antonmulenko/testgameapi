package com.game.core.models;

public class PalindromModel {

    private String userId;
    private String palindrom;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getPalindrom() {
        return palindrom;
    }

    public void setPalindrom(String palindrom) {
        this.palindrom = palindrom;
    }
}
