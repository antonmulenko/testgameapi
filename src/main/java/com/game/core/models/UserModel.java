package com.game.core.models;

import java.util.Collections;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

public class UserModel {

    private String userId;
    private int userScore;

    private volatile Set<String> palindromList = Collections.synchronizedSet(new HashSet<>());

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public int getUserScore() {
        return userScore;
    }

    public boolean addPalindrom(String palindrom) {
        if (checkPalindrom(palindrom)) {
            return palindromList.add(palindrom);
        }
        return false;
    }

    private boolean checkPalindrom(String palindromRaw) {
        //todo maybe remove specials chars (.replaceAll("<remove chars>", "");)
        String direct = palindromRaw.toLowerCase();
        String reverse = new StringBuffer(direct).reverse().toString();
        return Objects.equals(direct, reverse);
    }

    public void recalculate() {
        userScore = 0;
        for (String item : palindromList) {
            userScore += item.length();
        }
    }
}
