package com.game.core.config;

public class Config {
    public static final int DEFAULT_LEADERBOARD_LENGTH = 5;

    private int leaderboardLength = DEFAULT_LEADERBOARD_LENGTH;
    private String connectionString;


    public int getLeaderboardLength() {
        return leaderboardLength;
    }

    public void setLeaderboardLength(int leaderboardLength) {
        this.leaderboardLength = leaderboardLength;
    }

    public String getConnectionString() {
        return connectionString;
    }

    public void setConnectionString(String connectionString) {
        this.connectionString = connectionString;
    }
}
