package com.game.core.services;

import com.game.core.GameContext;
import com.game.core.models.PalindromModel;
import com.game.core.models.UserModel;
import com.game.core.models.UserScoreModel;

import java.util.List;

public class DefaultGamePalindromService implements PalindromService {
    @Override
    public boolean addPalindrom(PalindromModel palindromModel) {
        try {
            UserModel userModel =
                    GameContext.getInstance().getUsersRepository().getUserModel(palindromModel.getUserId());

            boolean result = userModel.addPalindrom(palindromModel.getPalindrom());
            if (!result) {
                return false;
            }

            userModel.recalculate();
            GameContext.getInstance().getUsersRepository().addUserModel(userModel);

            UserScoreModel userScoreModel = new UserScoreModel();
            userScoreModel.setUserId(userModel.getUserId());
            userScoreModel.setUserScore(userModel.getUserScore());

            GameContext.getInstance().getLeaderboardRepository().addUserScore(userScoreModel);

        } catch (Exception e) {
            System.out.println("error: " + e.getMessage());
            return false;
        }
        return true;
    }

    @Override
    public int getUserScore(String userId) {
        return GameContext.getInstance().getUsersRepository().getUserScore(userId);
    }

    @Override
    public List<UserScoreModel> getLeaderboard() {
        return GameContext.getInstance().getLeaderboardRepository().getLeaderboard();
    }
}
