package com.game.core.services;

import com.game.core.models.PalindromModel;
import com.game.core.models.UserScoreModel;

import java.util.List;

public interface PalindromService {
    boolean addPalindrom(PalindromModel palindromModel);

    int getUserScore(String userId);

    List<UserScoreModel> getLeaderboard();
}