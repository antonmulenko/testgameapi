package com.game.core;

import com.game.core.models.UserScoreModel;

import java.util.List;

public interface GameCoreApi {
    boolean addPalindrom(String userId, String palindrom);

    int getUserScore(String userId);

    List<UserScoreModel> getLeaderboard();
}
