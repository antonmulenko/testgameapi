package com.game.core;

import com.game.core.config.Config;
import com.game.core.repository.DefaultLeaderboardRepository;
import com.game.core.repository.DefaultUserRepository;
import com.game.core.repository.LeaderboardRepository;
import com.game.core.repository.UsersRepository;
import com.game.core.services.DefaultGamePalindromService;
import com.game.core.services.PalindromService;

public class GameContext {
    private static volatile GameContext instance;

    private Config config;
    private PalindromService palindromService;
    private UsersRepository usersRepository;
    private LeaderboardRepository leaderboardRepository;

    private GameContext() {
        if (instance != null) {
            throw new RuntimeException("Use getInstance() method to get the single instance of this class.");
        }
    }

    public static GameContext getInstance() {
        if (instance == null) {
            synchronized (GameContext.class) {
                if (instance == null) {
                    instance = new GameContext();
                }
            }
        }

        return instance;
    }

    public Config getConfig() {
        if (config == null) {
            config = new Config();
        }
        return config;
    }

    public void setConfig(Config config) {
        this.config = config;
    }

    public PalindromService getPalindromService() {
        if (palindromService == null) {
            palindromService = new DefaultGamePalindromService();
        }
        return palindromService;
    }

    public void setPalindromService(PalindromService palindromService) {
        this.palindromService = palindromService;
    }

    public UsersRepository getUsersRepository() {
        if (usersRepository == null) {
            usersRepository = new DefaultUserRepository();
        }
        return usersRepository;
    }

    public void setUsersRepository(UsersRepository usersRepository) {
        this.usersRepository = usersRepository;
    }

    public LeaderboardRepository getLeaderboardRepository() {
        if (leaderboardRepository == null) {
            leaderboardRepository = new DefaultLeaderboardRepository();
        }
        return leaderboardRepository;
    }

    public void setLeaderboardRepository(LeaderboardRepository leaderboardRepository) {
        this.leaderboardRepository = leaderboardRepository;
    }
}
