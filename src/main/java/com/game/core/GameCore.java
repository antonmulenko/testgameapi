package com.game.core;

import com.game.core.models.PalindromModel;
import com.game.core.models.UserScoreModel;

import java.util.List;

public class GameCore implements GameCoreApi {
    @Override
    public boolean addPalindrom(String userId, String palindrom) {
        PalindromModel palindromModel = new PalindromModel();
        palindromModel.setUserId(userId);
        palindromModel.setPalindrom(palindrom);
        return GameContext.getInstance().getPalindromService().addPalindrom(palindromModel);
    }

    @Override
    public int getUserScore(String userId) {
        return GameContext.getInstance().getPalindromService().getUserScore(userId);
    }

    @Override
    public List<UserScoreModel> getLeaderboard() {
        return GameContext.getInstance().getPalindromService().getLeaderboard();
    }
}
